#!/bin/bash

# Create a user for the specified user ID and group ID,
# to match the access rights of the host system user.
id "user" &>/dev/null
user=$?
set -e
if [[ $user -eq 1 ]]; then
  addgroup --gid $GID group
  adduser --uid $UID --gid $GID --disabled-password --gecos "" user
fi

# Create server SSH key to be shared with the monitor
if [[ ! -e /home/user/.ssh/id_ed25519 ]]; then
  sudo -i -u user bash -c "mkdir -p \$HOME/.ssh && ssh-keygen -t ed25519 -f \$HOME/.ssh/id_ed25519 && cp \$HOME/.ssh/id_ed25519.pub \$HOME/.ssh/authorized_keys"
fi

exec "$@"

