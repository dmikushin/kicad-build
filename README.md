# KiCad Builder and Repository

APT repository to host development releases of KiCad packages.

This set of containers does the following:

1. Monitors updates to the 5 key KiCad repositories: `kicad`, `kicad-symbols`, `kicad-footprints`, `kicad-templates`, `kicad-packages3d`.
2. Once an update is available, it is compiled (incrementally, if previous build files are available), and a Debian (.deb) package is created.
3. Debian package is published to the Aptly repository, older version of package is removed.

## Building

```
docker build -t kicad-build:ubuntu22.04 .
docker build -t kicad-build-monitor -f monitor/Dockerfile monitor
```

Aptly container must be also build from the `aptly` branch. Also, autossh container is used for port forwarding.

## Deployment

1. This script generates environment variables to set UID and GID equal to the host system user:

```
./env.sh
```

2. Additionally, `.env` file must provide the following definitions specific to the network configuration:

```
SSH_REMOTE_USER=
SSH_REMOTE_HOST=
SSH_REMOTE_PORT=
SSH_TUNNEL_PORT=
```

3. Working folders for containers must be created explicitly, otherwise docker will create them with wrong access rights (on behalf of the root user):

```
mkdir -p ccache build build-symbols build-footprints build-templates build-packages3d log log-symbols log-footprints log-templates log-packages3d user data data/public gnupg
```

4. Generate keys for the Aptly web service SSH tunneling:

```
ssh-keygen -t rsa -b 4096 -C "autossh" -f id_rsa
```

5. Finally, start the containers:

```
docker-compose up -d
```

