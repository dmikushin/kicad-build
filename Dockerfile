FROM kicad-source-containers:ubuntu22.04

RUN apt-get update && \
        apt-get -y --no-install-recommends install \
		software-properties-common && \
	apt-get clean

# Adding Git repository to install the latest git
# (see issue description here: https://stackoverflow.com/a/74762495/4063520)
RUN gpg --keyserver keyserver.ubuntu.com --recv-keys A1715D88E1DF1F24 && gpg --export A1715D88E1DF1F24 > /etc/apt/trusted.gpg.d/git.gpg && \
	apt-add-repository 'deb https://ppa.launchpadcontent.net/git-core/ppa/ubuntu focal main'

RUN apt-get update && \
	apt-get -y --no-install-recommends install \
		git && \
	apt-get clean

# Install mold as recommended here: https://dev-docs.kicad.org/en/build/linux/
RUN git clone https://github.com/rui314/mold.git && \
	mkdir mold/build && \
	cd mold/build && \
	git checkout v1.7.1 && \
	../install-build-deps.sh && \
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=c++ -G Ninja .. && \
	cmake --build . && \
	cmake --install .

# Remove the test script, which clones kikad sources into the container,
# because we won't do this.
RUN rm -rf /test-install.sh

# Install our new build script
COPY scripts/build.sh /

# Install SSH server
RUN apt-get update && \
        apt-get -y --no-install-recommends install \
                openssh-server \
		sudo \
		vim-tiny && \
        apt-get clean

# Allow sudo without password
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
RUN mkdir /var/run/sshd
RUN bash -c 'install -m755 <(printf "#!/bin/sh\nexit 0") /usr/sbin/policy-rc.d'
RUN ex +'%s/^#\zeListenAddress/\1/g' -scwq /etc/ssh/sshd_config
RUN ex +'%s/^#\zeHostKey .*ssh_host_.*_key/\1/g' -scwq /etc/ssh/sshd_config
RUN echo "PermitRootLogin prohibit-password" >>/etc/ssh/sshd_config
RUN RUNLEVEL=1 dpkg-reconfigure openssh-server
RUN ssh-keygen -A -v
RUN update-rc.d ssh defaults

# Entrypoint is used to create SSH keys in the shared folder
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

ENV UID "${UID}"
ENV GID "${GID}"

# Start SSH server
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D", "-o", "ListenAddress=0.0.0.0"]

