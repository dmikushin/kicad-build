#!/bin/bash
# $1 : kicad-packages3d
# $2 : https://gitlab.com/kicad/libraries/kicad-packages3d.git
set -e -x

name=$1
url=$2
force=$3

# Update our fork according to the upstream
cd /kicad/ThirdParty/$name
git config user.email "dmitry@kernelgen.org"
git config user.name "Dmitry Mikushin"
git remote add upstream $url || true
git fetch upstream
git rebase --abort || true
sha1_in=$(git describe --match=NeVeRmAtCh --always)
git rebase upstream/master
sha1_out=$(git describe --match=NeVeRmAtCh --always)
if [[ "$sha1_in" == "$sha1_out$force" ]]; then
  echo "No update since the last build, exiting"
  exit 0
fi

export CCACHE_BASEDIR=/kicad
export CCACHE_DIR=/ccache

build=$(echo "$name" | sed "s/kicad/build/g")
mkdir -p /$build
cd /$build

# Configure
cmake \
        -G Ninja \
        -DCMAKE_C_COMPILER_LAUNCHER=ccache \
        -DCMAKE_CXX_COMPILER_LAUNCHER=ccache \
        -DCMAKE_BUILD_TYPE=Release \
        -DKICAD_STDLIB_LIGHT_DEBUG=ON \
        -DKICAD_SCRIPTING_WXPYTHON=ON \
        -DKICAD_USE_OCC=ON \
        -DKICAD_SPICE=ON \
        -DKICAD_BUILD_I18N=ON \
        -DKICAD_BUILD_PNS_DEBUG_TOOL=ON \
        -DCMAKE_CXX_FLAGS=-fuse-ld=mold \
	/kicad/ThirdParty/$1

# Build
# TODO Attach git SHA1 to the log
log=$(echo "$name" | sed "s/kicad/log/g")
ninja 2>&1 | tee /$log/build_log.txt

# Report on the metrics of the code
cat /$log/build_log.txt | { grep "warning:" || test $? = 1; } | awk 'END{print "number_of_warnings "NR}' > /$log/metrics.txt
cat /$log/metrics.txt

# Package with CPack
cpack

