#!/usr/bin/env python3
import time
import os
import os.path
import re
import aptly

print("Starting monitoring process")

aptly_url = 'kicad-aptly-api:8080'
repo = 'kicad'

components = {
    'kicad' : 'https://gitlab.com/kicad/code/kicad.git',
    'kicad-symbols' : 'https://gitlab.com/kicad/libraries/kicad-symbols.git',
    'kicad-footprints' : 'https://gitlab.com/kicad/libraries/kicad-footprints.git',
    'kicad-templates' : 'https://gitlab.com/kicad/libraries/kicad-templates.git',
    'kicad-packages3d' : 'https://gitlab.com/kicad/libraries/kicad-packages3d.git',
}

def build(name, url, force=False):
    f = 'force' if force else ''
    os.system(f'ssh -o "StrictHostKeyChecking=no" kicad-build-server /build.sh "{name}" "{url}" {f}')

def publish(name, deb):
    # Publish to aptly, if newer
    aptly.deb_publish_update(name, deb, repo, aptly_url)

while True:

    for name, url in components.items():
        build_path = re.sub('kicad', '/build', name)
        deb = f'{build_path}/{name}.deb'
        if not os.path.isfile(deb):
            print(f'Creating {name}.deb for the first time, this may take a while...')
            build(name, url, force=True)
        else:
            build(name, url)

        publish(name, deb)

        time.sleep(60)

