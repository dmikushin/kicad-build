#!/bin/bash

# Create a user for the specified user ID and group ID,
# to match the access rights of the host system user.
id "user" &>/dev/null
user=$?
set -e
if [[ $user -eq 1 ]]; then
  addgroup --gid $GID group
  adduser --uid $UID --gid $GID --disabled-password --gecos "" user
fi

# https://github.com/moby/moby/issues/31243#issuecomment-406825071
usermod -a -G tty user

exec "$@"

