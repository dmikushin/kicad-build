#!/usr/bin/env python3
#
# This is a set of repository and package management functions
# based on direct use of Aptly API https://www.aptly.info/doc/api/repos/
#
# Could be also done with a third-party library, e.g. https://github.com/gopythongo/aptly-api-client
# but our code here is more compact and straight-forward.
#

import pycurl
import subprocess
import re
from io import BytesIO, StringIO
import json

def deb_get_version(deb):
    output = subprocess.check_output(f'dpkg-deb -I {deb}', shell=True).decode("utf-8")

    # Version: 6.0.10-d60e9a5990
    m = re.search(r'Version:\s([a-z0-9\.\-]+)', output)
    if m is None:
        print(f'Cannot parse {output}')
        return ''

    return m.group(1)

def deb_upload(name, deb, url):
    print(f'Uploading package {deb}...')
    c = pycurl.Curl()
    c.setopt(c.URL, f'http://{url}/api/files/{name}')
    c.setopt(c.POST, 1)
    c.setopt(c.HTTPPOST, [('file', (c.FORM_FILE, deb))])
    c.perform()
    c.close()

def deb_publish(name, repo, url):
    c = pycurl.Curl()
    c.setopt(c.URL, f'http://{url}/api/repos/{repo}/file/{name}?forceReplace=1')
    c.setopt(c.POST, 1)
    c.perform()
    c.close()

    # Published repository is not updated automatically, so we update it here.
    c = pycurl.Curl()
    c.setopt(c.URL, f'http://{url}/api/publish/:./ubuntu')
    c.setopt(c.POST, 1)
    c.setopt(c.CUSTOMREQUEST, 'PUT')
    c.perform()
    c.close()

def deb_get_published(name, repo, url):
    c = pycurl.Curl()
    c.setopt(c.URL, f'http://{url}/api/repos/{repo}/packages')
    buffer = BytesIO()
    c.setopt(c.WRITEFUNCTION, buffer.write)
    c.perform()
    c.close()

    results = json.loads(buffer.getvalue().decode("utf-8"))
    published = []
    for result in results:
        m = re.search(f'{name}\s([a-z0-9\.\-]+)\s([a-z0-9]+)', result)
        if m is None:
            continue

        published.append((name, m.group(1), m.group(2), result))

    return published

def deb_remove(name, key, repo, url):
    c = pycurl.Curl()
    c.setopt(c.URL, f'http://{url}/api/repos/{repo}/packages')
    c.setopt(c.POST, 1)
    c.setopt(c.CUSTOMREQUEST, 'DELETE')
    c.setopt(c.HTTPHEADER, ['Accept: application/json', 'Content-type: application/json'])
    body_as_dict = { 'PackageRefs' : [ key ] }
    body_as_json_string = json.dumps(body_as_dict)
    print(body_as_json_string)
    body_as_file_object = StringIO(body_as_json_string)
    c.setopt(c.READDATA, body_as_file_object) 
    c.setopt(c.POSTFIELDSIZE, len(body_as_json_string))
    c.setopt(c.VERBOSE, True)
    c.perform()
    c.close()

def deb_publish_update(name, deb, repo, url):
    version = deb_get_version(deb)
    print(f'Publishing package {name} version {version}...')
    published = deb_get_published(name, repo, url)

    update = True
    for package in published:
        if name != package[0]:
            continue

        if version == package[1]:
            print(f'Package {name} version {version} already published, nothing to update')
            update = False
        else:
            # Unpublish everything, which does not match our updated version
            key = package[3]
            print(f'Package {name} version {package[1]} key {key} is marked for removal')
            deb_remove(name, key, repo, url)

    if update:
        deb_upload(name, deb, url)
        deb_publish(name, repo, url)
        print(f'Package {name} version {version} published')

