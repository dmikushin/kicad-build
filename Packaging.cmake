find_package(Git REQUIRED)

# Find the nearest git tag
# https://stackoverflow.com/a/5261470/4063520
execute_process(COMMAND
	/bin/sh -c "\"${GIT_EXECUTABLE}\" for-each-ref refs/tags --sort=-taggerdate --format=\"%(tag)\" --count=1"
	WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
	OUTPUT_VARIABLE GIT_TAG
	OUTPUT_STRIP_TRAILING_WHITESPACE
	COMMAND_ERROR_IS_FATAL ANY)

# Find the commit's SHA1, and whether the building workspace was dirty or not
execute_process(COMMAND
	"${GIT_EXECUTABLE}" describe --match=NeVeRmAtCh --always --dirty
	WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
	OUTPUT_VARIABLE GIT_SHA1
	OUTPUT_STRIP_TRAILING_WHITESPACE
	COMMAND_ERROR_IS_FATAL ANY)

set(CPACK_GENERATOR "DEB")
set(CPACK_PACKAGE_NAME ${PROJECT_NAME})
set(CPACK_PACKAGE_VERSION "${GIT_TAG}-${GIT_SHA1}")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "KiCad Templates Library")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "libwxgtk3.0-gtk3-0v5, libodbc2")

set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Dmitry Mikushin <dmitry@kernelgen.org>")
set(CPACK_PACKAGING_INSTALL_PREFIX "usr")

include(CPack)

